
let http = require("http");

http.createServer(function(req,res){

	if(req.url == "/" && req.method == "GET") {
		res.writeHead(200,{"Content-Type": "text/plain"});
		res.end("Welcome to Booking system")

	} 
	if(req.url == "/profile" && req.method == "GET") {
		res.writeHead(200,{"Content-Type": "text/plain"});
		res.end("Welcome to your profile!")

	}
	if(req.url == "/courses" && req.method == "GET") {
		res.writeHead(200,{"Content-Type": "text/plain"});
		res.end("Here's our courses available");
	}
	if(req.url == "/addCourse" && req.method == "POST"){
		res.writeHead(200,{"Content-Type": "text/plain"});
		res.end("Add course to our resources");
	}
	if(req.url == "/updateCourse" && req.method == "PUT"){
		res.writeHead(200,{"Content-Type": "text/plain"});
		res.end("Update a Course to our resources");
	}
	if(req.url == "/archiveCourse" && req.method == "DELETE"){
		res.writeHead(200,{"Content-Type": "text/plain"});
		res.end("Archive Courses to our resources");

	}
}).listen(4000);

console.log("The server is now running on localhost:4000!");